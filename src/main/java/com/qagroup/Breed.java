package com.qagroup;

public enum Breed {
	
	BULLDOG("Bulldog"),
	PUDEL("Pudel");

	private String name;

	private Breed(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}

}

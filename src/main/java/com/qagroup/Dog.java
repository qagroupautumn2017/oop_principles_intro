package com.qagroup;

public class Dog extends Animal {
	
	private String nickname;

	public Dog() {
		super("Dog");
		this.nickname = "Sparky";
	}

	public Dog(String nick) {
		super("Dog");
		this.nickname = nick;
	}
	
	public Dog(Breed breed) {
		super("Dog");
	}

	public void bark() {
		System.out.println(toString() + " " + this.nickname + " is barking!");
	}
	
	public void bark(int times) {
		for(int i = 0; i < times; i++)
			System.out.println("Wooff!");
	}

	@Override
	public void move() {
		System.out.println(toString() + " " + this.nickname + " is running!");
	}
}

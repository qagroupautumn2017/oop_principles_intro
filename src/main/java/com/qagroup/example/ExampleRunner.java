package com.qagroup.example;

import java.util.Random;

import com.qagroup.Animal;
import com.qagroup.Breed;
import com.qagroup.Cat;
import com.qagroup.Dog;
import com.qagroup.Duck;
import com.qagroup.Shark;

public class ExampleRunner {

	public static void main(String[] args) {
		Dog dog = new Dog();
		dog.move();
		dog.bark();

		Duck duck = new Duck();
		duck.move();
		// duck.bark();

		Shark shark = new Shark();
		shark.move();
		// shark.bark();

		// dog = shark;
		// dog = new Duck();
		// dog = new Animal("Dog");

		Animal animal = getRandomAnimal();
		animal.move();
		// animal.bark();
		
		Object obj = new Dog();
		System.out.println(obj.toString());
		
		Dog dogRex = new Dog("Rex");
		dogRex.bark();
		
		dogRex = new Dog(Breed.BULLDOG);
		dogRex.bark(10);
	}

	public static Animal getRandomAnimal() {
		int rand = new Random().nextInt(4);
		switch (rand) {
		case 0:
			return new Dog();
		case 1:
			return new Duck();
		case 2:
			return new Shark();
		case 3:
			return new Cat();
		default:
			return null;
		}
	}

	public static void main2(String[] args) {
		Animal dog = new Animal("Dog");
		dog.move();

		Animal duck = new Animal("Duck");
		duck.move();
		Animal dack = new Animal("Dack");
		dack.move();

		Animal shark = new Animal("Shark");
		shark.move();

		dog = shark;
		dog.move();
		dog = new Animal("Shark");
	}
}

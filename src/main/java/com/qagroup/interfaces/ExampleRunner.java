package com.qagroup.interfaces;

import com.qagroup.Dog;

public class ExampleRunner {

	public static void main(String[] args) {
		 Car car = new Car("BMW");
		 car.move();
		 
		//
		// Movable movable = new Car("VW");
		// movable.move();

		start(new Car("Opel"));
		start(new Dog());
		start(new Star());
	}

	public static void start(Movable movable) {
		movable.move();
	}

}

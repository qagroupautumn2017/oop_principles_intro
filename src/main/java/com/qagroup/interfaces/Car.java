package com.qagroup.interfaces;

public class Car implements Movable {

	private String name;

	public Car(String name) {
		this.name = name;
	}

	public void move() {
		System.out.println("Car " + name + "  is moving");
	}

	@Override
	public String toString() {
		return this.name;
	}

}

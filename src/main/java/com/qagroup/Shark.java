package com.qagroup;

public class Shark extends Animal {

	public Shark() {
		super("Shark");
	}

	@Override
	public void move() {
		System.out.println(toString() + " is swimming!");
	}
}

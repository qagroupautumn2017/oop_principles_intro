package com.qagroup.equality;

import com.qagroup.Animal;

public class EqualityExamples {

	public static void main(String[] args) {
		int a1 = 4;
		int a2 = 4;
		
		System.out.println(a1 == a2);
		
		String s1 = new String("Java");
		String s2 = new String("Java");
		
		System.out.println(s1 == s2);
		System.out.println(s1.equals(s2));
		
		s1 = "Java";
		s2 = "Java";
		System.out.println(s1 == s2);
		
		Animal animal1 = new Animal("Dog");
		Animal animal2 = new Animal("Dog");
		
		System.out.println(animal1 == animal2);
		System.out.println(animal1.equals(animal2));
		
	}

}

package com.qagroup;

public class Cat extends Animal {

	public Cat() {
		super("Cat");
	}
	
	@Override
	public void move() {
		System.out.println(toString() + " is catching a mouse!");
	}
}

package com.qagroup;

import com.qagroup.interfaces.Movable;

public class Animal implements Movable {
	private String name;

	public Animal(String name) {
		this.name = name;
	}

	public void move() {
		System.out.println(this.name + " is moving!");
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Animal) {
			Animal that = (Animal) obj;

			return this.name.equals(that.name);
		} else
			return false;
	}
}